﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TCPClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string host;
            int port = 80;

            if (args.Length == 0)
                // If no server name is passed as argument to this program, 
                // use the current host name as the default.
                host = Dns.GetHostName();
            else
                host = args[0];

            host = "127.0.0.1";
            port = 8092;

            Console.WriteLine($"Client started, sending messages to {host}:{port}");
            while (true)
            {
                Console.Write("Input message: ");
                string message = Console.ReadLine();
                Console.WriteLine(SendData(host, port, message));
            }
        }

        private static string SendData(string server, int port, string message)
        {
            string result = SocketCommands.SendCommand(server, port, message);

            return $"Sended: \"{message}\"     -->     Result: \"{result}\"";
        }

    }
}
