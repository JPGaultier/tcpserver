﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TCPClient
{
    /// <summary>
    /// Envío de comandos por Socket.
    /// Cada comando establece la conexión con el socket, y en caso de no poderse,
    /// se realiza un número de reintentos (se puede cambiar), asegurando así
    /// la conexión en todo momento.
    /// </summary>
    /// <remarks>
    /// El primer comando a ejecutar es el de inicialización #I#. Este comando lo ejecutaremos una sola vez al inicio del programa y no volveremos a ejecutarlo en ningún otro momento, salvo si ejecutamos el comando #E#, el cual, cierra la conexión y finaliza la ejecución de CashlogyConnector.
    /// El comando #E# lo ejecutaremos justo antes de cerrar la aplicación T.P.V.
    /// </remarks>
    public class SocketCommands
    {
        private static int MAX_ATTEMPTS = 5;

        /// <summary>
        /// Change the maximum number of attempts to try when the socket connection failed.
        /// </summary>
        /// <param name="nAttempts">Maximum number of attempts.</param>
        public static void SetMaxAttempts(int nAttempts)
        {
            MAX_ATTEMPTS = nAttempts;
        }

        /// <summary>
        /// Get the maximum number of attempts to try before the socket connection failed.
        /// </summary>
        /// <returns>Maximum number of attempts.</returns>
        public static int GetMaxAttempts()
        {
            return MAX_ATTEMPTS;
        }

        /// <summary>
        /// Send a request using Socket.
        /// </summary>
        /// <param name="server">Host name/address of the server to send the request.</param>
        /// <param name="port">Server port where the server is listening.</param>
        /// <param name="request">Request message to send.</param>
        /// <returns>Result with the response of the server or the error.</returns>
        public static string SendCommand(string server, int port, string request)
        {
            string result;
            int attempts = 0;
            do
            {
                try
                {
                    result = SocketUtility.SocketSendReceive(server, port, request);
                    attempts = MAX_ATTEMPTS;
                }
                catch (SocketException err)
                {
                    result = string.Format("Client connection failed: {0}", err.Message);
                    attempts++;
                }
            } while (attempts < MAX_ATTEMPTS);

            return result;
        }

    }
}
