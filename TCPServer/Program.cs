﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCPServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string TcpListener_IP = "127.0.01";
            string TcpListener_Port = "8092";

            Thread t = new Thread(delegate ()
            {
                try
                {
                    // replace the IP with your system IP Address...
                    TCPServer myserver = new TCPServer(TcpListener_IP, int.Parse(TcpListener_Port));
                }
                catch (System.Net.Sockets.SocketException ex)
                {
                    Console.WriteLine($"Error in Server: {ex.Message}");
                    Console.WriteLine($"Probably the IP direction introduced in the config file is not correct. Please, check it using \"ipconfig\" and reinitialize this server.");
                    Console.ReadKey();
                }
                catch (System.FormatException ex)
                {
                    Console.WriteLine($"Error in Server: {ex.Message}");
                    Console.WriteLine($"Probably the IP direction introduced in the config file has an incorrect format. Please, change it and reinitialize this server.");
                    Console.ReadKey();
                }
            });
            t.Start();

            Console.WriteLine("Server Started...!");
        }
    }
}
